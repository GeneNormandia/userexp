extends Node2D

var observer_scene = load("res://Scenes/Enemies/Observer/Observer.tscn")
var asteroid_scene = load("res://Scenes/Enemies/Asteroid/Asteroid.tscn")
var ship_scene = load("res://Scenes/Enemies/Ship/Ship.tscn")

var entities
var player

var max_spawn_cooldown = 1
var spawn_cooldown = max_spawn_cooldown

# =====================Pickup Spawner===========================
var health_pickup_scene = load("res://Pickups/HealthPickup.tscn")
var oxygen_pickup_scene = load("res://Pickups/OxygenPickup.tscn")
var fireRate_pickup_scene = load("res://Pickups/FireRatePickup.tscn")
var max_pickup_spawner_cd = 5
var pickup_spawner_cd = max_pickup_spawner_cd
# ==============================================================

var edges = ["Left", "Right", "Up", "Down"]

func _ready():
	entities = $Entities
	player = entities.get_node("Player")

func _process(delta):
	if spawn_cooldown > 0:
		spawn_cooldown -= delta
	else:
		spawn_cooldown = max_spawn_cooldown
		var choice = edges[randi() % edges.size()]
		if choice == "Left":
			spawn_asteroid(Vector2(-640 * 2, rand_range(0, 720)) + player.global_position)
			spawn_ship(Vector2(-640 * 2, rand_range(0, 720)) + player.global_position)
		elif choice == "Right":
			spawn_asteroid(Vector2(640 * 2, rand_range(0, 720)) + player.global_position)
			spawn_ship(Vector2(640 * 2, rand_range(0, 720)) + player.global_position)
		elif choice == "Up":
			spawn_asteroid(Vector2(rand_range(0, 1280), -360 * 2) + player.global_position)
			spawn_ship(Vector2(rand_range(0, 1280), -360 * 2) + player.global_position)
		elif choice == "Down":
			spawn_asteroid(Vector2(rand_range(0, 1280), 360 * 2) + player.global_position)
			spawn_ship(Vector2(rand_range(0, 1280), 360 * 2) + player.global_position)
	
	# =====================Pickup Spawner===========================
	if pickup_spawner_cd > 0:
		pickup_spawner_cd -= delta
	else:
		pickup_spawner_cd = max_pickup_spawner_cd
		var pickup_type = randi() % 3
		spawn_pickup(pickup_type)
	# ==============================================================


# =====================Pickup Spawner===========================
func spawn_pickup(pickup_type):
	if (pickup_type == 0):
		var health_pickup = health_pickup_scene.instance()
		set_pickup_position(health_pickup)
		add_child(health_pickup)
		print("Health Pickup Spawned")
	elif (pickup_type == 1):
		var oxygen_pickup = oxygen_pickup_scene.instance()
		set_pickup_position(oxygen_pickup)
		add_child(oxygen_pickup)
		print("Oxygen Pickup Spawned")
	elif (pickup_type == 2):
		var fireRate_pickup = fireRate_pickup_scene.instance()
		set_pickup_position(fireRate_pickup)
		add_child(fireRate_pickup)
		print("Weapon Upgrade Pickup Spawned")
	pass

func set_pickup_position(pickup):
	var pickup_location = randi() % 4
	if pickup_location == 0:
		pickup.position = (Vector2(-640, rand_range(0, 720)) + player.global_position)
	elif pickup_location == 1:
		pickup.position = (Vector2(640, rand_range(0, 720)) + player.global_position)
	elif pickup_location == 2:
		pickup.position = (Vector2(rand_range(0, 1280), -360) + player.global_position)
	elif pickup_location == 3:
		pickup.position = (Vector2(rand_range(0, 1280), 360) + player.global_position)
	pass
# ==============================================================


func spawn(enemy_scene, position):
	var enemy = enemy_scene.instance()
	enemy.position = position
	enemy.target = player
	entities.add_child(enemy)
	pass

func spawn_asteroid(position, size = 3, speed = 200, angular_velocity = 1000):
	var enemy = asteroid_scene.instance()
	enemy.position = position
	enemy.size = size
	enemy.initial_speed = speed
	enemy.initial_angular_velocity = angular_velocity
	enemy.connect("spawn_asteroid", self, "spawn_asteroid")
	entities.call_deferred("add_child", enemy)
	print("bbbbbbbbbbbbbbb")
	pass
	

func spawn_ship(position, size = 3, speed = 200, angular_velocity = 1000):
	var enemy = ship_scene.instance()
	enemy.position = position
	enemy.size = size
	enemy.initial_speed = speed
	enemy.initial_angular_velocity = angular_velocity
	enemy.connect("spawn_ship", self, "spawn_ship")
	entities.call_deferred("add_child", enemy)
	print("AAAAAAAAAAAAAAAAA")
	pass


