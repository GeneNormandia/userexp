extends Area2D

var isDestroyed := false

func destroy():
	if isDestroyed:
		return
	
	isDestroyed = true
	
	get_parent().remove_child(self)
	queue_free()

func _on_FireRatePickup_body_entered(body):
	if body.is_in_group("Player"):
		body.upgradeWeapon()
		destroy()

func _on_Timer_timeout():
	print("fire rate pickup timed out!")
	destroy()
